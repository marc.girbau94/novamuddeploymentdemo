from novamud import Dungeon, Room


class BabyRoom(Room):
    pass


class BabyDungeon(Dungeon):

    def init_dungeon(self):
        hr = BabyRoom(self)
        self.start_room = hr


if __name__ == '__main__':
    import sys
    host, port = None, None
    if len(sys.argv) == 2:
        host, port = sys.argv[1].split(':')
    BabyDungeon(host, port).start_dungeon()
